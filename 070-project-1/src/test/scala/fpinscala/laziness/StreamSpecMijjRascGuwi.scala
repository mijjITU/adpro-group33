package fpinscala.laziness

import scala.language.higherKinds
import org.scalatest.FlatSpec
import org.scalatest.prop.Checkers
import org.scalacheck._
import org.scalacheck.Prop._
import Arbitrary.arbitrary
import fpinscala.laziness.stream00.Stream

//import stream00._    // uncomment to test the book solution
import stream01._ // uncomment to test the broken headOption implementation
//import stream02._ // uncomment to test another version that breaks headOption

class StreamSpecMijj extends FlatSpec with Checkers {

  import Stream._

  behavior of "headOption"

  //Should return None on an empty stream (already included in the examples)
  //Scenario test
  it should "return None on an empty Stream (01)" in {
    assert(empty.headOption == None)
  }

  //it should return some for a non-empty stream; (already included
  //  in the examples)
  def list2stream[A] (la :List[A]): Stream[A] =
    la.foldRight (empty[A]) (cons[A](_,_))

  // In ScalaTest we use the check method to switch to ScalaCheck's internal DSL
  def genNonEmptyStream[A] (implicit arbA :Arbitrary[A]) :Gen[Stream[A]] =
    for { la <- arbitrary[List[A]] suchThat (_.nonEmpty)}
      yield list2stream (la)

  //def infiniteStream[A] (implicit arbA :Arbitrary[A]) : Gen[Stream[A]] = ???

  //Property test
  it should "return the head of the stream packaged in Some (02)" in check {
    // the implict makes the generator available in the context
    implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    ("singleton" |:
      Prop.forAll { (n :Int) => cons (n,empty).headOption == Some (n) } ) &&
      ("random" |:
        Prop.forAll { (s :Stream[Int]) => s.headOption != None } )

  }

  //headOption should not force the tail of the stream
  it should "not force the tail of the stream" in {
    cons(1, Stream(throw new RuntimeException("The tail was forced"))).headOption
  }



  behavior of "take"

  // take should not force any heads nor any tails of the Stream it
  //manipulates
  it should "not force any heads nor any tails of the Stream it manipulates" in
    {
    //Test for the head
    cons(throw new RuntimeException("The head was forced"),
      Stream(throw new RuntimeException("The tail was forced"))).take(1)
    //Test for the tail
    cons(throw new RuntimeException("The head was forced"),
      Stream(throw new RuntimeException("The tail was forced"))).take(2)
  }

  //take(n) does not force (n+1)st head ever (even if we force all
  //elements of take(n))
  it should "not force (n+1)st head ever (even if we force all elements of " +
    "take(n)" in check {
    implicit def arbPostiveInt = Arbitrary[Int] (Gen.choose(0, 1000))
    Prop.forAll {n: Int =>
      val naturals = from(0)
      val s1 = cons(throw new RuntimeException("forced the n+1"),
        Stream(throw new RuntimeException("forced the n+1")))
      val s2 = naturals.take(n).append(s1)
      s2.take(n).toList == naturals.take(n).toList
    }

  }

  //- s.take(n).take(n) == s.take(n) for any Stream s and any n
  //  (idempotency)
  it should "s.take(n).take(n) == s.take(n) for any Stream s and any n" in check
  {
    implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    implicit def arbPostiveInt = Arbitrary[Int] (Gen.choose(0, 1000))

    Prop.forAll{ (s: Stream[Int], n: Int) =>
      s.take(n).take(n).toList == s.take(n).toList }

  }

  behavior of "drop"

  // s.drop(n).drop(m) == s.drop(n+m) for any n, m (additivity)
  it should "s.drop(n).drop(m) == s.drop(n+m) for any n, m (additivity)" in
    check {
    implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    implicit def arbPostiveInt = Arbitrary[Int] (Gen.choose(0, 1000))

    Prop.forAll{ (s: Stream[Int], n: Int, m: Int) =>
      s.drop(n).drop(m) == s.drop(n + m) }

  }
  // s.drop(n) does not force any of the dropped elements heads
  // the above should hold even if we force some stuff in the tail
  it should "not force any of the dropped elements heads" in check {
    implicit def arbPostiveInt = Arbitrary[Int] (Gen.choose(1, 1000))
    Prop.forAll{ (n :Int) => {
      val naturals = from(0)
      val s1 = cons(throw new RuntimeException("dropped elements heads"), naturals)
      s1.drop(n).take(n).toList
      true
    }
    }
  }


  behavior of "map"

  //- x.map(id) == x (where id is the identity function)
  it should "x.map(id) == x (where id is the identity function)" in check {
    implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    Prop.forAll{x: Stream[Int] => x.map(m => m).toList == x.toList}
  }

  //- map terminates on infinite streams
  it should "terminate on infinite streams" in {
    val naturals: Stream[Int] = from(0)
    naturals.map(m => m)
  }

  behavior of "append"

  //Appending empty stream (a) to another stream (b), should be the stream (b)
  it should "A stream of nothing append should = stream " in check {
    implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    Prop.forAll{s: Stream[Int] => empty.append(s).toList == s.toList }
  }

  //Appending a stream with length of n to a stream with length of m, should be a
  // stream of length  n+m
  it should "s.take(n) appended with s.take(m) length = n+m" in check {
    //implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])
    implicit def arbPostiveInt = Arbitrary[Int] (Gen.choose(0, 100))
    val naturals: Stream[Int] = from(0)

    Prop.forAll{(n: Int, m: Int) =>
      naturals.take(n).append(naturals.take(m)).toList.size == n+m }
  }
  //Appending two stream does not force the tail
  //Sidenote: Originaly I would have said that it would'nt force the head
  //either, but it seems it does for the first stream I'm not sure why?
  it should "not force the tail" in {
    val s1 = cons(1, Stream(throw new RuntimeException("The tail in stream1 was forced")))
    val s2 = cons(throw new RuntimeException("the head in stream2 was forced"),
      Stream(throw new RuntimeException("The tail in stream2 was forced")))
    s1.append(s2)
    true
  }


  // a(b)(c)== a(b(c))
  it should "a(b)(c)== a(b(c))" in check {
    implicit def arbIntStream = Arbitrary[Stream[Int]] (genNonEmptyStream[Int])

    Prop.forAll{(s1: Stream[Int], s2: Stream[Int], s3: Stream[Int]) =>
      s1.append(s2).append(s3).toList == s1.append(s2.append(s3)).toList}
  }

  it should "streamOne.Append(StreamOne) equals StreamOne x 2" in {
    val li = (1 to 2).toStream
    val li2 = List(1, 2, 1, 2).toStream
    assert(li.append(li).toList == li2.toList)
  }

  it should "empty.append(empty).headOption ==  None" in {
    assert(empty.append(empty).headOption == None)
  }
}
