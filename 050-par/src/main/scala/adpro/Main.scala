// Advanced Programming
// Andrzej Wąsowski, IT University of Copenhagen

//package adpro
import java.awt.Choice

import adpro._
import Par._
import java.util.concurrent.Executors


//See "Main" as a Java-like "main" method.
object Main extends App {

    println("Welcome to Parallel Computation, the ADPRO 050 class!!")
    private val es = Executors.newWorkStealingPool()
    // play with your functions below.
    //Test
    //Exercises 1
    val q = unit (1) (es)
    println(q.get)
    val a = asyncF((x: Int) => x + 1)
    val testAsyncF = Par.run (es) (a(5))
    println("Test async : " + testAsyncF.get())

    //Exercises 2
    val list = List(unit(1),unit(2),unit(3))
    val b = sequence(list)
    val testSeq = Par.run (es) (b)
    println("Test sequence : " + testSeq.get())

    //Exercises 3
    val c = parFilter(list) ( x=> x == UnitFuture(2))
    val testFilter = Par.run (es) (c)
    println("Test parFilter : " + testFilter.get())

    //Exercises 4
    val d = map3(unit(1),unit(2),unit(3)) (_ + _ + _)
    val testMap3 = Par.run (es) (d)
    println("Test Map3 : " + testMap3.get())

    val d2 = map3(unit(1),unit(2),unit(3)) (_ + _ + _)
    val testMap32 = Par.run (es) (d2)
    println("Test Map32 : " + testMap32.get())

    //Exercises 5
    val e = choiceN(unit(2)) (list)
    val testChoiceN = Par.run (es) (e)
    println("Test ChoiceN : " + testChoiceN.get())

    val f = choice(unit(true))(unit(1), unit(2))
    val testChoice = Par.run(es)(f)
    println("Test Choise : " + testChoice.get())

    //Exercises 6
    val g = chooser(unit(2))(list)
    val testChooser = Par.run(es) (g)
    println("Test Chooser : " + testChooser.get())

    val h = choiceNviaChooser(unit(2))(list)
    val testChoiceNviaChooser = Par.run(es) (h)
    println("Test choiceNViaChooser : " + testChoiceNviaChooser.get() )


    val i = choiceViaChooser(unit(true) )(unit(1), unit(2))
    val testChoiceViaChooser = Par.run(es) (i)
    println("Test choiceViaChooser : " + testChoiceViaChooser.get())


}
