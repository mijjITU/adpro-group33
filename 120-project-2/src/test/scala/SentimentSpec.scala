import Main.ParsedReview
import org.scalatest.{BeforeAndAfterAll, FreeSpec, Matchers}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Dataset
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql._


class SentimentSpec extends FreeSpec with Matchers with BeforeAndAfterAll {

  org.apache.log4j.Logger getLogger "org"  setLevel (org.apache.log4j.Level.WARN)
  org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)

  val spark =  SparkSession.builder
    .appName ("Sentiment")
    .master  ("local[12]")
    .getOrCreate

  override def afterAll = spark.stop

  import spark.implicits._

  val glove  = Main.loadGlove ("glove/glove.6B.50d.txt")
  val reviews = Main.loadReviews ("Musical_Instruments_5.json")
  val tokenized = Main.tokenize(reviews)
  val translated = Main.translateWords(tokenized)
  val joined = Main.joinData(translated, glove)
  val scoresRemapped = Main.changeOverall(joined)
  val averaged = Main.sumAndAverage(scoresRemapped)

  "put your tests here"   - {

    "nested tests group" - {
      //Test antal af rows er det samme sum and average
      "individual test #1" in {
        val joinCount = joined.count()
        val scoresCount = scoresRemapped.count()
        assert(scoresCount == joinCount)
      }

      //check that no reviews have a score over 2.0 (changeOverall)
      "individual test #2" in {
        val countLow = scoresRemapped.filter(x => x._2< 0).count()
        val countHigh = scoresRemapped.filter(x => x._2 > 2).count()
        assert(countHigh == 0 && countLow == 0)
      }

      //check if we split arrays in correct proportions
      "individual test #3" in {
        val sentence = "this was a good movie yes"
        val dataset = Seq((1.asInstanceOf[Integer], "this was a good movie yes", 1.asInstanceOf[Double])).toDS()
          .withColumnRenamed("_2", "text")
          .as[ParsedReview]

        val tokenize = Main.tokenize(dataset)

        val splitSenteceCount = sentence.split("\\s+").count(_ => true)

        System.out.println("splitSenteceCount" + splitSenteceCount)

        val tokenizeCount = tokenize.head()._4.count(_ => true)

        System.out.println(tokenize.head()._4)

        assert(splitSenteceCount == tokenizeCount)
      }

      "individual test #4" in {
        val getColumns = averaged.columns.toSeq
        assert(getColumns.length == 3)
      }

      "individual test #5" in {
        val dataset = Seq((1.asInstanceOf[Integer], 1.0, Array(0.1, 0.2, 0.3), 1.asInstanceOf[Integer]),
                         (1.asInstanceOf[Integer], 1.0, Array(0.3, 0.2, 0.1), 1.asInstanceOf[Integer])).toDS()
        val vec = Vectors.dense(0.2,0.2,0.2)
        val result = Seq((1.asInstanceOf[Integer], 1.0, vec)).toDF("id", "label", "features")
        val sumAvg = Main.sumAndAverage(dataset)

        assert(sumAvg.head().get(2) == result.head().get(2))


      }
    }

  }

}
