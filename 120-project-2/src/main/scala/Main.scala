// Advanced Programming. Andrzej Wasowski. IT University
// To execute this example, run "sbt run" or "sbt test" in the root dir of the project
// Spark needs not to be installed (sbt takes care of it)
// By Rasmus Christensen, Gustav Winberg and Mikkel Johansen

import org.apache.spark.ml.feature.{Tokenizer, Word2Vec}
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator


object Main {

	type Embedding       = (String, List[Double])
	type ParsedReview    = (Integer, String, Double)

	org.apache.log4j.Logger getLogger "org"  setLevel (org.apache.log4j.Level.WARN)
	org.apache.log4j.Logger getLogger "akka" setLevel (org.apache.log4j.Level.WARN)
	val spark =  SparkSession.builder
		.appName ("Sentiment")
		.master  ("local[9]")
		.getOrCreate

  import spark.implicits._

	val reviewSchema = StructType(Array(
			StructField ("reviewText", StringType, nullable=false),
			StructField ("overall",    DoubleType, nullable=false),
			StructField ("summary",    StringType, nullable=false)))

	// Read file and merge the text abd summary into a single text column

	def loadReviews (path: String): Dataset[ParsedReview] =
		spark
			.read
			.schema (reviewSchema)
			.json (path)
			.rdd
			.zipWithUniqueId
			.map[(Integer,String,Double)] { case (row,id) => (id.toInt, s"${row getString 2} ${row getString 0}", row getDouble 1) }
			.toDS
			.withColumnRenamed ("_1", "id" )
			.withColumnRenamed ("_2", "text")
			.withColumnRenamed ("_3", "overall")
			.as[ParsedReview]

  // Load the GLoVe embeddings file

  def loadGlove (path: String): Dataset[Embedding] =
		spark
			.read
			.text (path)
      .map  { _ getString 0 split " " }
      .map  (r => (r.head, r.tail.toList.map (_.toDouble))) // yuck!
			.withColumnRenamed ("_1", "word" )
			.withColumnRenamed ("_2", "vec")
			.as[Embedding]

	//First Tokenize the text (Turn it into words)
	def tokenize(reviews: Dataset[ParsedReview]) = {
		val tokenizer = new Tokenizer().setInputCol("text").setOutputCol("words")
		val wordsData = tokenizer.transform(reviews).as[(Integer, String, Double, Array[String])]
		wordsData
	}

	// translate all the words in a reviewText and the summary to vectors from the GLoVe dictionary.
	def translateWords(token : Dataset[(Integer, String, Double, Array[String])]) = {
		token.flatMap {
			case (id, text, overall, words) => words.map(word =>
				(id, if (overall > 3.0) 2.0 else if (overall == 3.0) 1 else 0, word))
			//Renaming
		}.withColumnRenamed("_1", "id").withColumnRenamed("_2", "overall").withColumnRenamed("_3", "word")
	}

	def joinData(words: DataFrame, glove: Dataset[Embedding]) = {
		words.join(glove, words("word") === glove("word")).drop("word")
			//Add count to get average
			.withColumn("count", lit(1))
			.as[(Integer, Double, Array[Double], Integer)]
	}

	/*
	def changeOverall(dataset: Dataset[(Integer, Double, Array[Double], Integer)]) = {
		dataset.map {
			case (id, overall, vec, count) =>
				(id, if (overall > 3.0) 2.0 else if (overall == 3.0) 1 else 0, vec, count)
		}
	}*/

	//We sum all vectors for given review and divide the result by the number of vectors summed (compute the average vector).
	// Ignore the words that are not found in the GLoVe corpus.
	def sumAndAverage(data : Dataset[(Integer, Double, Array[Double], Integer)])= {


		val sum = data.groupByKey(_._1).reduceGroups((x,y) =>
			(x._1, x._2,
				x._3.zip(y._3).map(a => a._1 + a._2)
				,x._4 + y._4
			)
		).map(_._2)

		val avg = sum.map(x => (x._1,x._2, Vectors.dense(x._3.map(d => d / x._4))))
			//Renaming
			.withColumnRenamed("_1", "id")
			.withColumnRenamed("_2", "label")
			.withColumnRenamed("_3", "features")
		avg
	}

	type Result = (List[Double],List[Double],List[Double],List[Double])

	def perceptron(k: Int, dataFrame: DataFrame) : Result = {
		val splits = dataFrame.randomSplit(Array(0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1), seed = 1234L)

		def loop (n : Int, res: Result) : Result = {
			if (n == k) res
			else {
				val train = splits.filter(x => x != n).reduce(_ union _)
				val test = splits(n)

				val layers = Array[Int](50, 5, 4,3)

				val trainer = new MultilayerPerceptronClassifier()
					.setLayers(layers)
					.setBlockSize(128)
					.setSeed(1234L)
					.setMaxIter(4)

				val model = trainer.fit(train)

				val result = model.transform(test)
				val predictionAndLabels = result.select("prediction", "label")
				val evaluator = new MulticlassClassificationEvaluator()
					.setMetricName("accuracy")
				val e = evaluator.evaluate(predictionAndLabels)
				val precision = evaluator.setMetricName("weightedPrecision").evaluate(predictionAndLabels)
				val recall = evaluator.setMetricName("weightedRecall").evaluate(predictionAndLabels)
				val f1 = evaluator.setMetricName("f1").evaluate(predictionAndLabels)


				println(s"Test set accuracy = ${e}")
				println(s"Test set precision = ${precision}")
				println(s"Test set recall = ${recall}")
				println(s"Test set f1 = ${f1}")
				loop(n+1, (e::res._1, precision::res._2, recall::res._3, f1::res._4))
			}

		}
		loop(0, (List.empty, List.empty, List.empty, List.empty))
	}

	def mean (l : List[Double]) = {
		if (l.isEmpty) None
		else Some(l.sum / l.length)
	}

	def variance(xs: List[Double]) : Option[Double] = {
		mean(xs).flatMap(m => mean(xs.map(x => Math.pow(x-m,2))))
	}

  def main(args: Array[String]) = {
		///Users/mikkel/Documents/Advanced programming/ExercisesGroup33/120-project-2/glove/glove.6B.50d.txt
    val glove  = loadGlove ("glove/glove.6B.50d.txt")
		// /Users/mikkel/Documents/Advanced programming/ExercisesGroup33/120-project-2/Musical_Instruments_5.json
    val reviews = loadReviews ("reviews_Digital_Music_5.json")

		val a = tokenize(reviews)

		val b = translateWords(a)

		val c = joinData(b, glove)

		val e = sumAndAverage(c)

		val time = System.nanoTime()
		val f = perceptron(9,e)
		val finish = (System.nanoTime() - time) / 1e9d

		val avgAccuracy = mean(f._1)
		val avgPrecision = mean(f._2)
		val avgRecall = mean(f._3)
		val avgF1 = mean(f._4)

		val varian = variance(f._1)

		println(s"Final test Accuracy = ${avgAccuracy.get}")
		println(s"Final test Precision = ${avgPrecision.get}")
		println(s"Final test Recall = ${avgRecall.get}")
		println(s"Final test F1 = ${avgF1.get}")
		println(s"Final test Variance = ${varian.get}")
		println(s"Time = ${finish} seconds OR ${finish/60} minutes" )

		spark.stop
  }

}
