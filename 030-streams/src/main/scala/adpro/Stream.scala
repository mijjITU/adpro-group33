// Advanced Programming
// Andrzej Wąsowski, IT University of Copenhagen
//
// meant to be compiled, for example: fsc Stream.scala
//
// By Rasmus Christensen, Gustav Winberg and Mikkel Johansen
package adpro

sealed trait Stream[+A] {
  import Stream._

  def headOption () :Option[A] =
    this match {
      case Empty => None
      case Cons(h,t) => Some(h())
    }

  def tail :Stream[A] = this match {
      case Empty => Empty
      case Cons(h,t) => t()
  }

  def foldRight[B] (z : =>B) (f :(A, =>B) => B) :B = this match {
      case Empty => z
      case Cons (h,t) => f (h(), t().foldRight (z) (f))
      // Note 1. f can return without forcing the tail
      // Note 2. this is not tail recursive (stack-safe) It uses a lot of stack
      // if f requires to go deeply into the stream. So folds sometimes may be
      // less useful than in the strict case
    }

  // Note 1. eager; cannot be used to work with infinite streams. So foldRight
  // is more useful with streams (somewhat opposite to strict lists)
  def foldLeft[B] (z : =>B) (f :(A, =>B) =>B) :B = this match {
      case Empty => z
      case Cons (h,t) => t().foldLeft (f (h(),z)) (f)
      // Note 2. even if f does not force z, foldLeft will continue to recurse
    }

  def exists (p : A => Boolean) :Boolean = this match {
      case Empty => false
      case Cons (h,t) => p(h()) || t().exists (p)
      // Note 1. lazy; tail is never forced if satisfying element found this is
      // because || is non-strict
      // Note 2. this is also tail recursive (because of the special semantics
      // of ||)
    }


  //Exercise 2
  def toList: List[A] = {
    def loop(s: Stream[A], acc: List[A]): List[A] =
    s match {
      case Empty => acc
      case Cons(h,t) => loop(t(), h() :: acc )
    }
    loop(this, List()).reverse
  }

  //Exercise 3
  //
  //Try the following test case (should terminate with no memory exceptions and very fast). Why?
  //naturals.take(1000000000).drop(41).take(10).toList
  //The answer is first generated when a computation looks at the elements of the resulting stream and the stream will only do just enough work to generate the requested elements.
  //
  def take(n: Int): Stream[A] = {
    this match {
      case Empty               => empty
      case Cons(h,t) if n == 1 => cons(h(),empty)
      case Cons(h,t)           => cons(h(), t().take(n-1))

    }
  }

  //@annotation.tailrec
  def drop(n: Int): Stream[A] =
    this match {
      case Empty                => empty
      case Cons(h,t) if n == 1  => t()
      case Cons(h,t)            => t().drop(n-1)
  }

  //Exercise 4
  /*
  Test your implementation on the following test case:
    naturals.takeWhile.(_<1000000000).drop(100).take(50).toList
  It should terminate very fast, with no exceptions thrown. Why?

  It do not terminate at all, because of the . between takeWhile and (_<1000000000) :P
  But besides that, the interleaved transformation with drop and takeWhile and the laziness of the stream makes it to only evaluate the necessary values
  */
  def takeWhile(p: A => Boolean): Stream[A] = {
    this match {
      case Cons(h,t) if p(h()) => cons(h(), t().takeWhile(p))
      case _                   => empty
    }
  }


  //Exercise 5
  /*
    Thisshouldsucceed: naturals.forAll (_ < 0)
    This should crash: naturals.forAll (_ >=0) . Explain why.

    cd 030-streams
    sbt console
    import adpro._
    imprt Stream._
    naturals
    adpro.Stream[Int] = Cons(adpro.Stream$$$Lambda$4078/344969793@2d933b91,adpro.Stream$$$Lambda$4079/12293272@21c5999)

    naturals.


    Because of the stricness the first one never have to evaluate any of the numbers
    and the second one have to run through all the natural numbers to infinity and therefor get a stackOverflow


    Recall that exists has already been implemented before (in the book).
    Both forAll and exists are a bit strange for infinite streams;
    you should not use them unless you know the result;
    but once you know the result there is no need to use them.
    They are fine to use on finite streams. Why?

    Because it will terminate on finite streams even if it have to check the hole stream, where it might never terminate on infinite streams because it just keep checking forever
  */
  def forAll(p: A => Boolean): Boolean = {
    foldRight(true)((h,t) => p(h) && t)
  }


  //Exercise 6
  def takeWhile2(p: A => Boolean): Stream[A] = {
    foldRight(empty[A])((h,t) =>
      if (p(h))  cons(h,t)
      else       empty )
  }

  //Exercise 7
  //
  //Devise a couple of suitable test cases using infinite streams. you can reuse naturals.
  // testcase 1: val l1 = from(100)
  // testcase 2: val l2 : Stream[Int] = cons(1,l2)
  def headOption2 () :Option[A] = {
    foldRight(None: Option[A])((h,_) => Some(h))
  }

  //Exercise 8 The types of these functions are omitted as they are a part of the exercises
  //There is alot of  test cases to do


  def map[B] (f: A => B) : Stream[B] = {
    foldRight(empty[B])((h,t) => cons(f(h),t))
  }
  def filter (f: A => Boolean) : Stream[A] = {
    foldRight(empty[A])((h,t) =>
      if(f(h)) cons(h,t)
      else t )
  }
  def  append[B>:A] (s: => Stream[B]) : Stream[B] = {
    foldRight(s)((h,t) => cons(h,t))
  }
  def flatMap[B] (f: A => Stream[B]) : Stream[B] = {
    foldRight(empty[B])((h,t) => f(h).append(t) )

  }

  //Exercise 09
  //Put your answer here: Because for a stream it will stop when it hit the first element that matches and for a list it will transform the hole list with the filter function first.

  //Exercise 10
  //Put your answer here:

  //Exercise 11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = {
    f(z) match {
      case None => empty
      case Some((h,t)) => cons(h,unfold(z)(f))
    }
  }


  //Exercise 12
  def fib2 : Stream[Int] = {
    unfold((0,1))( x => Some(x._1, (x._2, x._1 + x._2) ) )
  }
  def from2 (n: Int) = {
    unfold(n)(x => Some((x,x+1)))
  }

  //Exercise 13
  def map2[B] (f : A => B) : Stream[B] = {
    unfold(this){
      case Cons(h,t) => Some(f(h()) ,t())
      case Empty     => None
    }
  }
  def take2(n: Int) : Stream[A] = {
    unfold(this,n){
      case (Cons(h,t),1)            => Some(h(), (empty,0))
      case (Cons(h,t), n) if n > 1  => Some((h(), (t(), n-1)))
      case _ => None
    }

  }
  def takeWhile3(f: A => Boolean) =
    unfold(this){
      case Cons(h, t) if f(h()) => Some(h(), t())
      case _ => None
  }
  def zipWith2[B,C](s: Stream[B])(f: (A,B) => C): Stream[C] = {
    unfold((this, s)) {
      case (Cons(h1, t1), Cons(h2, t2)) =>
        Some((f(h1(), h2()), (t1(), t2())))
      case _ => None
    }
  }

  /*
    What should be the result of this?
      naturals.map (_%2==0).zipWith[Boolean,Boolean] (naturals.map (_%2==1)) (_||_)
    .take(10).toList

    List(true, true, true, true, true, true, true, true, true, true)

  */

}


case object Empty extends Stream[Nothing]
case class Cons[+A](h: ()=>A, t: ()=>Stream[A]) extends Stream[A]

object Stream {

  def empty[A]: Stream[A] = Empty

  def cons[A] (hd: => A, tl: => Stream[A]) :Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def apply[A] (as: A*) :Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))
    // Note 1: ":_*" tells Scala to treat a list as multiple params
    // Note 2: pattern matching with :: does not seem to work with Seq, so we
    //         use a generic function API of Seq


  //Exercise 1
  def from(n:Int):Stream[Int]=cons(n,from(n+1))

  def to(n:Int):Stream[Int]= cons(n,from(n-1))

  val naturals: Stream[Int] = from(0)


}
