// Advanced Programming, Exercises by A. Wąsowski, IT University of Copenhagen
//
// AUTHOR1:
// AUTHOR2:
//
// Write ITU email addresses of both group members that contributed to
// the solution of the exercise (in lexicographic order).
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// The file is meant to be compiled inside sbt, using the 'compile' command.
// To run the compiled file use the 'run' or 'runMain' command.
// To load the file int the REPL use the 'console' command.
// Now you can interactively experiment with your code.
//
// Continue solving exercises in the order presented in the PDF file. The file
// shall always compile, run, and pass tests, after you are done with each
// exercise (if you do them in order).  Please compile and test frequently.

// The extension of App allows writing the main method statements at the top
// level (the so called default constructor). For App objects they will be
// executed as if they were placed in the main method in Java.
package fpinscala

object Exercises extends App with ExercisesInterface {

  import fpinscala.List._

  // Exercise 3

  // add @annotation.tailrec to make the compiler check that your solution is
  // tail recursive

  def fib (n: Int) : Int = {
    @annotation.tailrec
    def loop(n : Int, prev: Int, cur: Int): Int = {
      if (n == 0) prev
      else loop(n - 1, cur, prev + cur)
    }
    loop(n,0,1)
  }

  // Exercise 4

  // add @annotation.tailrec to make the compiler check that your solution is
  // tail recursive
  def isSorted[A] (as: Array[A], ordered: (A,A) =>  Boolean) :Boolean = {
    @annotation.tailrec
    def loop (i : Int) :Boolean =
      if (i >= as.length -1) true
      else if (!ordered(as(i),as(i+1))) false
      else loop(i+1)
    loop(0)
  }

  // Exercise 5

  def curry[A,B,C] (f: (A,B)=>C): A => (B => C) = {
    (a : A) => (b: B) => f(a,b)
  }

  // Exercise 6

  def uncurry[A,B,C] (f: A => B => C): (A,B) => C = {
    (a: A, b: B) => f(a)(b)
  }

  // Exercise 7

  def compose[A,B,C] (f: B => C, g: A => B) : A => C = {
    a => f(g(a))
  }

  // Exercise 8 requires no programming
  //3

  // Exercise 9

  def tail[A] (as: List[A]) :List[A] = {
    as match {
      case Nil => sys.error("tail on empty list")
      case Cons(h,t) => t
    }
  }

  // Exercise 10

  //@annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  @annotation.tailrec
  def drop[A] (l: List[A], n: Int) : List[A] = {
    l match {
      case Nil => sys.error("drop on empty list")
      case Cons(h,t) if n == 1 => t
      case Cons(h,t) => drop(t,n-1)
    }
  }

  // Exercise 11

  // @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  @annotation.tailrec
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = {
    l match {
      case Cons(h,t) if f(h) => dropWhile(t,f)
      case _ => l
    }
  }

  // Exercise 12

  def init[A](l: List[A]): List[A] = {
    l match {
      case Nil => sys.error("init empty list")
      case Cons(_,Nil) => Nil
      case Cons(h,t) => Cons(h,init(t))
    }
  }

  // Exercise 13

  def length[A] (as: List[A]): Int = {
    foldRight(as, 0)((_,acc) => acc+1)
  }

  // Exercise 14

  @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  def foldLeft[A,B] (as: List[A], z: B) (f: (B, A) => B): B = {
    as match {
      case Nil => z
      case Cons(h,t) => foldLeft(t, f(z,h))(f)
    }
  }

  // Exercise 15

  def product (as: List[Int]): Int = {
    as match {
      case Nil => 1
      case Cons(h,t) => foldLeft(as,0)((x,y) => (x)+(y))
    }
  }

  def length1 (as: List[Int]): Int = {
    as match {
      case Nil => 0
      case Cons(h,t) => foldLeft(as,0)((acc,h) => acc+1 )
    }
  }

  // Exercise 16

  def reverse[A] (as: List[A]): List[A] = {
    foldLeft(as,List[A]()) ((acc,h) => Cons(h,acc))
  }

  // Exercise 17

  def foldRight1[A,B] (as: List[A], z: B) (f: (A, B) => B): B = {

    foldLeft(reverse(as),z)((x,y) => f(y,x))
  }

  // Exercise 18

  def foldLeft1[A,B] (as: List[A], z: B) (f: (B,A) => B): B = {
    foldRight[A,B => B] (as, (b: B) => b) ((a,g) => b => g(f(b,a)))(z)
  }

  // Exercise 19

  def append[A](a1: List[A], a2: List[A]): List[A] =
    a1 match {
    case Nil => a2
    case Cons(h,t) => Cons(h, append(t, a2))
  }

  def concat[A] (as: List[List[A]]): List[A] = {
    foldRight1(as, List[A]())((x,y) => append(x,y))
  }
  // Exercise 20

  def filter[A] (as: List[A]) (f: A => Boolean): List[A] = {
    foldRight1(as, List[A]())((h,t) =>
      if (f(h)) Cons(h,t)
      else t
    )
  }

  // Exercise 21

  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = {
    foldRight1(as,List[B]()) ((h,t) => append(f(h),t))
  }

  // Exercise 22

  def filter1[A] (l: List[A]) (p: A => Boolean) :List[A] = ???

  // Exercise 23

  def add (l: List[Int]) (r: List[Int]): List[Int] = ???
  /*{
    (l,r) match {
      case (_, Nil) => Nil
      case (Nil, _) => Nil
      case (Cons(h1,t1), Cons(h2,t2)) => Cons(h1+h2, add(t1,t2))
    }
  }
  Not working
  */


  // Exercise 24

  def zipWith[A,B,C] (f: (A,B)=>C) (l: List[A], r: List[B]): List[C] = ???

  // Exercise 25

  def hasSubsequence[A] (sup: List[A], sub: List[A]): Boolean = ???

  // Exercise 26

  def pascal (n: Int): List[Int] = ???

}
