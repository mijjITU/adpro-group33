 //package adpro

import adpro._
import RNG.SimpleRNG
import State._

object UseExample extends App {

val r0 = random_int // generator of random integers
val (i1,r1) = random_int.run (SimpleRNG(42))
val (i2,r2) = random_int.run (r1)
val (i3,r3) = random_int.run (r2)

//test 
//exercises 1
val (i4,r4) = RNG.nonNegativeInt(r3)
println(i4)
  /*
//exercises 2
val (d1, r5) = double(r4)
println(d1)
//exercises 3
val ((i5,d2),r6) = intDouble(r5)
val ((d3,i6), r7) = doubleInt(r6)
//exercises 4
val (l,r8) = ints(10,r7)
//Exercises 5
val d = _double(r8)
//exrcises 6

//exercises 7
val l1 = _ints(10)
*/
}
